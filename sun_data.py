from astral import LocationInfo
from pprint import pprint
import datetime
from astral.sun import sun
import pytz
from utils import interpolate
TZ = pytz.timezone('US/Pacific')

def get_sun_data():
    city = LocationInfo(name="San Francisco", region="California", timezone="America/Los_Angeles", latitude="37.77", longitude=-122.42)    
    s_today = sun(city.observer, date=datetime.date.today(), tzinfo=city.timezone)
    s_tmrrw = sun(city.observer, date=datetime.date.today()+datetime.timedelta(days = 1), tzinfo=city.timezone)
    now = datetime.datetime.now(tz=TZ)
    data = {
        'dawn': s_today['dawn'] if s_today['dawn'] > now else s_tmrrw['dawn'],
        'sunrise': s_today['sunrise'] if s_today['sunrise'] > now else s_tmrrw['sunrise'],
        'noon': s_today['noon'] if s_today['noon'] > now else s_tmrrw['noon'],
        'sunset': s_today['sunset'] if s_today['sunset'] > now else s_tmrrw['sunset'],
        'dusk': s_today['dusk'] if s_today['dusk'] > now else s_tmrrw['dusk'],
    }
    return data

def mins_until_sunrise():
    s = get_sun_data()
    sunrise_time = s['sunrise']
    remaining = sunrise_time - datetime.datetime.now(tz=TZ)
    mins = int(remaining.seconds/60)
    if mins > 60 * 22:  # 2 hour grace period at full brightness
        mins = 0
    return mins

def mins_after_sunset():
    s = get_sun_data()
    sunset_time = s['sunset']
    print(sunset_time)
    lapsed = sunset_time - datetime.datetime.now(tz=TZ)
    mins = int(lapsed.seconds/60)
    if mins > 60 * 22:  # 2 hour grace period at full brightness
        mins = 0
    return mins

# UPPER_BOUND = 800
# color_morphs = {  # Colors to shift between a (key) hrs before sunrise
#     UPPER_BOUND: (0,0,0,0),
#     240: (0,0,0,0),
#     180: (20,0,0,5),
#     120: (50,0,0,20),
#     60: (100,50,10,100),
#     0: (255,200,100,255),
# }

# mins_remaining = mins_until_sunrise()
# mins_remaining = 15
# print(f"{mins_remaining} minutes until sunrise")

# keys = sorted(list(color_morphs.keys())[:])
# lower_bound = 0
# upper_bound = UPPER_BOUND
# for i in keys:
#     # print(i)
#     if mins_remaining < i:
#         upper_bound = i
#         break
#     lower_bound = i

# print(lower_bound, upper_bound)

# a = color_morphs[lower_bound]
# b = color_morphs[upper_bound]
# #                                        Edge case where upper, lower both = 800
# r = (mins_remaining - lower_bound) / (upper_bound - lower_bound) if a != b else 0
# print(a)
# print(b)
# print(r)

# lerp = interpolate(a, b, r)

# print(lerp)

# print(mins_after_sunset())
# print(mins_until_sunrise())