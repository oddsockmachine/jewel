from program_color import ColorProgram
from time import sleep

class PastelProgram(ColorProgram):
    def __init__(self, neopixel_handle, length=7):
        super().__init__(neopixel_handle, length)
        self.scheme = [
            (208, 130, 130, 0),
            (233, 164, 117, 0),
            (228, 206, 128, 0),
            (148, 196, 153, 0),
            (113, 157, 214, 0),
        ]
    def step(self):
        for j in range(5):
            cs = self.scheme[j]
            for l in range(int(self.length/7)):
                self.buffer[(7*j)+l] = cs
   