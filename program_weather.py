from program_color import ColorProgram
from time import sleep

class WeatherProgram(ColorProgram):
    def __init__(self, neopixel_handle, length=7):
        super().__init__(neopixel_handle, length)
        self.state = None
        self.weathers = ['clouds', 'rain', 'storm', 'sun', 'snow']
    def step(self):
        # print(f"{self.name} empty play function")
        # self.display()
        # sleep(1)
        return


# {"id":804,"weather":"Clouds","detail":"overcast clouds","icon":"04d","tempk":285.65,"tempc":12.5,"temp_maxc":13.8,"temp_minc":11.1,"humidity":89,"pressure":1020,"maxtemp":287.04,"mintemp":284.26,"windspeed":2.68,"winddirection":226,"location":"San Francisco County","sunrise":1607872633,"sunset":1607907100,"clouds":100,"description":"The weather in San Francisco County at coordinates: 37.78, -122.46 is Clouds (overcast clouds)."}
