from program_color import ColorProgram
from time import sleep
from sun_data import mins_after_sunset
from utils import interpolate

class SunsetProgram(ColorProgram):
    def __init__(self, neopixel_handle, length=7):
        super().__init__(neopixel_handle, length)
        self.UPPER_BOUND = 800
        self.color_morphs = {  # Colors to shift between a (key) hrs before sunset
            self.UPPER_BOUND: (0,0,0,0),
            240: (0,0,0,0),
            180: (20,0,0,5),
            120: (50,0,0,20),
            60: (100,50,10,100),
            0: (255,200,100,255),
        }

    def step(self):
        c = self.get_color_for_sunset()
        for i in range(self.length):
            self.buffer[i] = c
        
    # TODO Fade in to soft lighting from sunset/dusk time

    def get_color_for_sunset(self):
        mins_remaining = mins_until_sunset()
        keys = sorted(list(self.color_morphs.keys())[:])
        lower_bound = 0
        upper_bound = self.UPPER_BOUND
        for i in keys:
            if mins_remaining < i:
                upper_bound = i
                break
            lower_bound = i
        a = self.color_morphs[lower_bound]
        b = self.color_morphs[upper_bound]
        #                                        Edge case where upper, lower both = 800
        r = (mins_remaining - lower_bound) / (upper_bound - lower_bound) if a != b else 0
        lerp = interpolate(a, b, r)
        return lerp

