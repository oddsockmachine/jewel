from actor import ActorThread
from time import sleep
from utils import gamma

class ColorProgram():
    def __init__(self, neopixel_handle, length=7):
        # super().__init__()
        self.length = length
        self.buffer = [(0,0,0,0) for i in range(self.length)]
        self.enabled = False
        return
    def enable(self):
        self.enabled = True
        return
    def disable(self):
        self.enabled = False
    def event_loop(self):
        # sleep(1)
        if self.enabled:
            self.step()
            return self.buffer
        else:
            return None



    # def display(self):
    #     if self.enabled:  # avoid two programs writing at once
    #         try:
    #             for i in range(self.length):
    #                 self.neopixel[i] = gamma(self.buffer[i])
    #             # print(self.neopixel[:4])
    #             self.neopixel.show()
    #         except:
    #             pass