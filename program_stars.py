from program_color import ColorProgram
from time import sleep
from datetime import datetime
from utils import fade, interpolate
from random import randint, choices, choice

class StarsProgram(ColorProgram):
    def __init__(self, neopixel_handle, length=7):
        super().__init__(neopixel_handle, length)
        self.fade_colors = [(238,153,17, 50),  	(240,185,4, 50),  	(107,35,4, 50), (0, 0, 0, 20), 	(210,111,4, 50)]
        self.night_colors = {
            (0,0,0,123): 1,  # violet
            (133,89,136,0):    10,  # violet
            (107,73,132,0):    10,  # purple
            (72,52,117,0):     10,  # purple
            (43,47,119,0):     20,  # blue
            (20,24,82,0):      20,  # dark blue
            (7,11,52,0):       20,  # dark
            (6,29,55,0 ):      20,  # dark
            (16,40,73,0):      20,  # dark
            (53,71,95,0):      10,  # light blue
            (36,43,75,0):      20,  # dark
            (22,27,54,0):      20,  # dark
            (11,16,38,0):      20,  # dark
        }
        self.fades = [fade((0,0,0,0),choice(list(self.night_colors.keys())),100,100) for x in range(self.length)]
        self.aurora_mode = False
        self.aurora_start_time = 0
        self.aurora_duration_seconds = 10
        self.aurora_every_x_seconds = 200
        self.aurora_position = 0

    def step(self):
        # TODO: morph between different color palettes depending on time
        # eg: sunset > twilight > night > possibility of auroras
        for i, f in enumerate(self.fades):
            if f.remaining == 0:
                f.last = f.next
                f.next = choices(list(self.night_colors.keys()), weights=list(self.night_colors.values()))[0]
                t = self.night_colors[f.next]
                f.steps = randint(t,t*10)  # More common colors last longer
                f.remaining = f.steps
            else:
                f.remaining -= 1
            r = 1-(f.remaining / f.steps)
            c = interpolate(f.last, f.next, r)
            self.buffer[i] = c
        if self.aurora_mode:
            
            duration = (datetime.now()-self.aurora_start_time).total_seconds()
            if duration >= self.aurora_duration_seconds:
                self.aurora_mode = False
            for i in range(self.length):
                self.buffer[i] = (255,255,255,255)
        else:
            self.mode_changer()    
        return

    def mode_changer(self):
        if self.aurora_mode:
            return
        i = randint(0,(self.aurora_every_x_seconds*50))
        # if i == 1:
        #     self.aurora_mode = True
        #     self.aurora_start_time = datetime.now()