from program_color import ColorProgram
from time import sleep
from utils import fade, interpolate
from random import randint, choice

class CandleProgram(ColorProgram):
    def __init__(self, neopixel_handle, length=7):
        super().__init__(neopixel_handle, length)
        self.flame_colors = [(238,153,17, 50),  	(240,185,4, 50),  	(107,35,4, 50), (0, 0, 0, 20), 	(210,111,4, 50)]
        self.flames = [fade((0,0,0,0),choice(self.flame_colors),100,100) for x in range(self.length)]
    
    def step(self):
        for i, f in enumerate(self.flames):
            if f.remaining == 0:
                f.last = f.next
                f.next = choice(self.flame_colors)
                f.steps = randint(5,50)
                f.remaining = f.steps
            else:
                f.remaining -= 1
            r = 1-(f.remaining / f.steps)
            c = interpolate(f.last, f.next, r)
            self.buffer[i] = c
        return

class FireProgram(ColorProgram):
    def __init__(self, neopixel_handle, length=7):
        super().__init__(neopixel_handle, length)
        self.flame_colors = [(100, 17, 0, 100), (150, 34, 3, 90), (215, 53, 2, 80), (252, 100, 0, 70), (255, 117, 0, 0), (250, 192, 0, 60)]
        self.flames = [fade((0,0,0,0),choice(self.flame_colors),100,100) for x in range(self.length)]
    
    def step(self):
        for i, f in enumerate(self.flames):
            if f.remaining == 0:
                f.last = f.next
                f.next = choice(self.flame_colors)
                f.steps = randint(3,30)
                f.remaining = f.steps
            else:
                f.remaining -= 1
            r = 1-(f.remaining / f.steps)
            c = interpolate(f.last, f.next, r)
            self.buffer[i] = c
        return