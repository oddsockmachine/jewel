from time import sleep
from json import loads
from datetime import datetime
from random import randint, choice

from pprint import pprint
from actor import ActorThread
from program_color import ColorProgram
from program_uptime import UptimeProgram
from program_candle import CandleProgram, FireProgram
from program_stars import StarsProgram
from program_rainbow import RainbowProgram, CycleProgram, PrismProgram
from program_pastel import PastelProgram
from program_sunrise import SunriseProgram
from program_sunset import SunsetProgram
from program_weather import WeatherProgram
from program_low import LowProgram
from program_off import OffProgram
from sun_data import get_sun_data, mins_until_sunrise
from utils import color_wheel, mock_neopixel, interpolate, gamma

import paho.mqtt.client as mqtt

NUM_JEWELS = 5
NUM_LEDS = 7 * NUM_JEWELS
import board
import neopixel
pixels = neopixel.NeoPixel(board.D18, NUM_LEDS, auto_write=False, pixel_order=neopixel.GRBW)
# pixels = [mock_neopixel() for x in range(NUM_LEDS)]
state = "low"
data = {}
CHANGE = False
msg = {}
FPS = 60

def display(buffer):
    # try:
    for i in range(NUM_LEDS):
        pixels[i] = gamma(buffer[i])
    # print(self.neopixel[:4])
    pixels.show()
    # except :
    #     print("!")
    #     pass
    return


# msgs = {
#     program: off | weather | candle | flux | low | color | notify | xmas | rainbow | clock  # Color mode to run
#     duration: n seconds/minutes/hours  # time until revert behavior triggered
#     revert: true | false  # Back to previous program, or just turn off after?
#     data : {
#         R/G/B
#         Brightness
#         misc
#     }
# }
# TODO extra layer of filters to shift gradually between two programs

def sub_cb(client, userdata, msg):
    payload = loads(msg.payload)
    if payload.get('state') in state_lookup.keys():
        global CHANGE
        global state
        global data
        CHANGE = True
        state = payload['state']
        data = payload.get('data', {})
        print("Changing state")

client = mqtt.Client("jewel")
client.connect('jarvis.local')
# client.connect('0.0.0.0')
client.message_callback_add("jewel", sub_cb)
client.subscribe("jewel")
client.loop_start()


class RGBWProgram(ColorProgram):
    def __init__(self, neopixel_handle, length=7):
        super().__init__(neopixel_handle, length)
    def step(self):
        # sleep(1)
        global data
        R = data.get("R")
        G = data.get("G")
        B = data.get("B")
        W = data.get("W")
        for i in range(self.length):
            self.buffer[i] = (R, G, B, W) 
        # self.display()


state_lookup = {
    "candle":  CandleProgram(None, NUM_LEDS),
    "fire":    FireProgram(None, NUM_LEDS),
    "sunset":  SunsetProgram(None, NUM_LEDS),
    "sunrise": SunriseProgram(None, NUM_LEDS),
    "low":     LowProgram(None, NUM_LEDS),
    "rainbow": RainbowProgram(None, NUM_LEDS),
    "stars":   StarsProgram(None, NUM_LEDS),
    "cycle":   CycleProgram(None, NUM_LEDS),
    "prism":   PrismProgram(None, NUM_LEDS),
    "off":     OffProgram(None, NUM_LEDS),
    "weather": WeatherProgram(None, NUM_LEDS),
    "RGBW":    RGBWProgram(None, NUM_LEDS),
    "pastel":  PastelProgram(None, NUM_LEDS),
    "color":   None,
    "notify":  None,
    "clock":   None,
    "xmas":    None,
    "waves":   None,
}

uptimer = UptimeProgram(client).start()
while True:
    program = state_lookup.get(state, None)
    program.enable()
    while not CHANGE:
        sleep(1.0/FPS)
        buffer = program.event_loop()
        if not buffer:
            continue
        display(buffer)
    program.disable()
    print(f"Changing state to {state}")
    CHANGE = False

