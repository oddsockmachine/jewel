from program_color import ColorProgram
from time import sleep

class LowProgram(ColorProgram):
    def __init__(self, neopixel_handle, length=7):
        super().__init__(neopixel_handle, length)
    def step(self):
        for i in range(self.length):
            self.buffer[i] = (50, 50, 50, 50) 
