from program_color import ColorProgram
from time import sleep
from utils import color_wheel

class RainbowProgram(ColorProgram):
    def __init__(self, neopixel_handle, length=7):
        super().__init__(neopixel_handle, length)
        self.j = 0
        self.speed = 0.1
    def step(self):
        self.j += self.speed
        self.j %= 255
        for i in range(self.length):
            pixel_index = (i * 256 // self.length) + self.j
            self.buffer[i] = color_wheel(int(pixel_index) & 255)
        return


class CycleProgram(ColorProgram):
    def __init__(self, neopixel_handle, length=7):
        super().__init__(neopixel_handle, length)
        self.j = 0
        self.speed = 0.1
    def step(self):
        self.j += self.speed
        self.j %= 255
        for y in range(int(self.length/7)):
            for i in range(7):
                pixel_index = (i * 256 // self.length) + self.j
                self.buffer[(y*7)+i] = color_wheel(int(pixel_index) & 255)
        return



class PrismProgram(ColorProgram):
    def __init__(self, neopixel_handle, length=7):
        super().__init__(neopixel_handle, length)
        self.j = 0
        self.speed = 0.3
    def step(self):
        self.j += self.speed
        self.j %= 255
        for y in range(int(self.length/7)):
            for i in range(7):
                pixel_index = (i * 256 // 7) + self.j
                self.buffer[(y*7)+i] = color_wheel(int(pixel_index) & 255)
        return