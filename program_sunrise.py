from program_color import ColorProgram
from time import sleep
from sun_data import mins_until_sunrise
from utils import interpolate

class SunriseProgram(ColorProgram):
    def __init__(self, neopixel_handle, length=7):
        super().__init__(neopixel_handle, length)
        self.UPPER_BOUND = 800
        self.color_morphs = {  # Colors to shift between a (key) hrs before sunrise
            self.UPPER_BOUND: (0,0,0,0),
            60: (0,0,0,0),
            45: (20,0,0,5),
            30: (50,0,0,20),
            15: (100,50,10,100),
            0: (255,200,100,255),
        }

    def step(self):
        c = self.get_color_for_sunrise()
        for i in range(self.length):
            self.buffer[i] = c

    def get_color_for_sunrise(self):
        mins_remaining = mins_until_sunrise()
        keys = sorted(list(self.color_morphs.keys())[:])
        lower_bound = 0
        upper_bound = self.UPPER_BOUND
        for i in keys:
            if mins_remaining < i:
                upper_bound = i
                break
            lower_bound = i
        a = self.color_morphs[lower_bound]
        b = self.color_morphs[upper_bound]
        #                                        Edge case where upper, lower both = 800
        r = (mins_remaining - lower_bound) / (upper_bound - lower_bound) if a != b else 0
        lerp = interpolate(a, b, r)
        return lerp

