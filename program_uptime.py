from actor import ActorThread
from datetime import datetime
from time import sleep

class UptimeProgram(ActorThread):
    def __init__(self, client, topic="uptime/jewel"):
        super().__init__()
        self.topic = topic
        self.client = client
        self.start_time = datetime.now()
        return
    def event_loop(self):
        runtime = str(datetime.now() - self.start_time).split('.')[0]
        # print(runtime)
        self.client.publish(self.topic, runtime)
        sleep(30)
