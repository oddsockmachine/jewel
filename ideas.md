# Operation

MQTT input

5x neopixel jewel output

# Modes

- Candle/fire
- Warm white
- Low red light for super late night
- Sunrise (and sunset?)
- Show approx time at night? (0 lights = 12am, 1 faint light = 1am, etc)
- Notification (flash a pattern when something happens)
- Gentle swirling rainbow
- Twinkling/shimmering stars
- waves

desktop controller with RGB encoder (and oled?) to set mode manually